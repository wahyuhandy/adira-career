module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: 'adira',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'adira career' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: 'https://use.fontawesome.com/releases/v5.1.0/css/all.css' },
    ],
    script: [
      { src: 'https://code.jquery.com/jquery-2.2.0.min.js', body: true, },
      { src: 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js', body: true, },
      { src: 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js', body: true, },
      { src: 'https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js', body: true, },
    ]
  },
  env: {
    apiAdira: 'https://www.kalibrr.id/api/companies/adira-finance/',
    baseApi: 'https://www.kalibrr.id/api/',
    localApi: 'https://id-embed.kalibrr.id/companies/adira-finance/'
  },
  /**
   * SCSS Loader
   */
  css:
    [
      '@/assets/css/style.css',
      '@/assets/scss/style.scss',
      '@/assets/css/OverlayScrollbars.min.css',
      '@/assets/css/slick.css',
      '@/assets/css/slick-theme.css'
    ],
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#3A3A3C' },
  /*
  ** Build configuration
  */
  modules: [
    '@nuxtjs/axios',
  ],
  axios: {
    credentials: true, 
    proxy: true,
    baseURL: 'https://www.kalibrr.id',
    browserBaseURL: 'https://www.kalibrr.id',
    https: true,
    debug: false
  },
  proxy: {
    '/api/*': 'https://www.kalibrr.id',
  },
  build: {
    /*
    ** Run ESLint on save
    */
    extend (config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    },
    extractCSS: true,
  },
  /**
   * Router Conf
   */
  router: {
    base: '/karir/'
  },
  /**
   * Plugins
   */
  plugins: [
    { src: '~/plugins/plugins', ssr: false }
  ]
}
